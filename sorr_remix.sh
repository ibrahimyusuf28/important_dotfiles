#!/bin/sh

dir="$HOME/Documents/SORRv5"
cd "${dir}/mod"
ln -s ost/remix music
ln -s ost/remix_namesong.txt namesong.txt
cd ..
wine SorR.exe
cd mod
rm music
rm namesong.txt
