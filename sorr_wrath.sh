#!/bin/sh

dir="$HOME/Documents/SORRv5"
cd "${dir}/mod"
ln -s ost/wrath music
ln -s ost/wrath_namesong.txt namesong.txt
cd ..
wine SorR.exe
cd mod
rm music
rm namesong.txt
